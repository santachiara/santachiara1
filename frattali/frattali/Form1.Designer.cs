﻿namespace frattali
{
    partial class Form1
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuovoProgettoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esportaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esportaFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salvaProgettoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Controlli = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pixelStepTextBox = new System.Windows.Forms.TrackBar();
            this.iterationCountTextBox = new System.Windows.Forms.TextBox();
            this.undoButton = new System.Windows.Forms.Button();
            this.generatePatternButton = new System.Windows.Forms.Button();
            this.xMaxCheckBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.xMinCheckBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.yMinCheckBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.yMaxCheckBox = new System.Windows.Forms.TextBox();
            this.zoomTextBox = new System.Windows.Forms.TrackBar();
            this.zoomCheckbox = new System.Windows.Forms.CheckBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.Controlli.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pixelStepTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTextBox)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(857, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuovoProgettoToolStripMenuItem,
            this.esportaToolStripMenuItem,
            this.salvaProgettoToolStripMenuItem,
            this.esciToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // nuovoProgettoToolStripMenuItem
            // 
            this.nuovoProgettoToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("nuovoProgettoToolStripMenuItem.Image")));
            this.nuovoProgettoToolStripMenuItem.Name = "nuovoProgettoToolStripMenuItem";
            this.nuovoProgettoToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.nuovoProgettoToolStripMenuItem.Text = "Nuovo Progetto";
            this.nuovoProgettoToolStripMenuItem.Click += new System.EventHandler(this.nuovoProgettoToolStripMenuItem_Click);
            // 
            // esportaToolStripMenuItem
            // 
            this.esportaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.esportaFileToolStripMenuItem});
            this.esportaToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("esportaToolStripMenuItem.Image")));
            this.esportaToolStripMenuItem.Name = "esportaToolStripMenuItem";
            this.esportaToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.esportaToolStripMenuItem.Text = "Esporta";
            // 
            // esportaFileToolStripMenuItem
            // 
            this.esportaFileToolStripMenuItem.Name = "esportaFileToolStripMenuItem";
            this.esportaFileToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.esportaFileToolStripMenuItem.Text = "Esporta file";
            // 
            // salvaProgettoToolStripMenuItem
            // 
            this.salvaProgettoToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("salvaProgettoToolStripMenuItem.Image")));
            this.salvaProgettoToolStripMenuItem.Name = "salvaProgettoToolStripMenuItem";
            this.salvaProgettoToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.salvaProgettoToolStripMenuItem.Text = "Salva progetto";
            // 
            // esciToolStripMenuItem
            // 
            this.esciToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("esciToolStripMenuItem.Image")));
            this.esciToolStripMenuItem.Name = "esciToolStripMenuItem";
            this.esciToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.esciToolStripMenuItem.Text = "Esci";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(0, 27);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.pictureBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.Controlli);
            this.splitContainer1.Size = new System.Drawing.Size(857, 474);
            this.splitContainer1.SplitterDistance = 645;
            this.splitContainer1.TabIndex = 3;
            this.splitContainer1.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(639, 468);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // Controlli
            // 
            this.Controlli.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Controlli.Controls.Add(this.zoomCheckbox);
            this.Controlli.Controls.Add(this.zoomTextBox);
            this.Controlli.Controls.Add(this.undoButton);
            this.Controlli.Controls.Add(this.generatePatternButton);
            this.Controlli.Controls.Add(this.xMaxCheckBox);
            this.Controlli.Controls.Add(this.label3);
            this.Controlli.Controls.Add(this.xMinCheckBox);
            this.Controlli.Controls.Add(this.label4);
            this.Controlli.Controls.Add(this.label6);
            this.Controlli.Controls.Add(this.yMinCheckBox);
            this.Controlli.Controls.Add(this.label5);
            this.Controlli.Controls.Add(this.yMaxCheckBox);
            this.Controlli.Controls.Add(this.iterationCountTextBox);
            this.Controlli.Controls.Add(this.label2);
            this.Controlli.Controls.Add(this.label1);
            this.Controlli.Controls.Add(this.pixelStepTextBox);
            this.Controlli.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Controlli.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Controlli.Location = new System.Drawing.Point(3, 3);
            this.Controlli.Name = "Controlli";
            this.Controlli.Size = new System.Drawing.Size(202, 471);
            this.Controlli.TabIndex = 2;
            this.Controlli.TabStop = false;
            this.Controlli.Text = "Controlli";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Iterazioni Frattale";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Pixel";
            // 
            // pixelStepTextBox
            // 
            this.pixelStepTextBox.Location = new System.Drawing.Point(7, 53);
            this.pixelStepTextBox.Minimum = 1;
            this.pixelStepTextBox.Name = "pixelStepTextBox";
            this.pixelStepTextBox.Size = new System.Drawing.Size(104, 45);
            this.pixelStepTextBox.TabIndex = 0;
            this.pixelStepTextBox.Value = 1;
            // 
            // iterationCountTextBox
            // 
            this.iterationCountTextBox.Location = new System.Drawing.Point(6, 126);
            this.iterationCountTextBox.Name = "iterationCountTextBox";
            this.iterationCountTextBox.Size = new System.Drawing.Size(103, 20);
            this.iterationCountTextBox.TabIndex = 5;
            this.iterationCountTextBox.Text = "85";
            // 
            // undoButton
            // 
            this.undoButton.BackColor = System.Drawing.Color.Transparent;
            this.undoButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.undoButton.ImageKey = "(none)";
            this.undoButton.Location = new System.Drawing.Point(10, 353);
            this.undoButton.Name = "undoButton";
            this.undoButton.Size = new System.Drawing.Size(41, 41);
            this.undoButton.TabIndex = 37;
            this.undoButton.UseVisualStyleBackColor = false;
            // 
            // generatePatternButton
            // 
            this.generatePatternButton.BackColor = System.Drawing.SystemColors.Control;
            this.generatePatternButton.Location = new System.Drawing.Point(57, 338);
            this.generatePatternButton.Name = "generatePatternButton";
            this.generatePatternButton.Size = new System.Drawing.Size(81, 57);
            this.generatePatternButton.TabIndex = 27;
            this.generatePatternButton.Text = "Generate Pattern";
            this.generatePatternButton.UseVisualStyleBackColor = false;
            // 
            // xMaxCheckBox
            // 
            this.xMaxCheckBox.Location = new System.Drawing.Point(67, 219);
            this.xMaxCheckBox.Name = "xMaxCheckBox";
            this.xMaxCheckBox.Size = new System.Drawing.Size(56, 20);
            this.xMaxCheckBox.TabIndex = 35;
            this.xMaxCheckBox.Text = "1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 154);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "yMin";
            // 
            // xMinCheckBox
            // 
            this.xMinCheckBox.Location = new System.Drawing.Point(8, 219);
            this.xMinCheckBox.Name = "xMinCheckBox";
            this.xMinCheckBox.Size = new System.Drawing.Size(56, 20);
            this.xMinCheckBox.TabIndex = 34;
            this.xMinCheckBox.Text = "-2";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(67, 154);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 29;
            this.label4.Text = "yMax";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(68, 203);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 13);
            this.label6.TabIndex = 33;
            this.label6.Text = "xMax";
            // 
            // yMinCheckBox
            // 
            this.yMinCheckBox.Location = new System.Drawing.Point(8, 170);
            this.yMinCheckBox.Name = "yMinCheckBox";
            this.yMinCheckBox.Size = new System.Drawing.Size(56, 20);
            this.yMinCheckBox.TabIndex = 30;
            this.yMinCheckBox.Text = "-1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 202);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "xMin";
            // 
            // yMaxCheckBox
            // 
            this.yMaxCheckBox.Location = new System.Drawing.Point(67, 170);
            this.yMaxCheckBox.Name = "yMaxCheckBox";
            this.yMaxCheckBox.Size = new System.Drawing.Size(56, 20);
            this.yMaxCheckBox.TabIndex = 31;
            this.yMaxCheckBox.Text = "1";
            // 
            // zoomTextBox
            // 
            this.zoomTextBox.Location = new System.Drawing.Point(10, 277);
            this.zoomTextBox.Minimum = 1;
            this.zoomTextBox.Name = "zoomTextBox";
            this.zoomTextBox.Size = new System.Drawing.Size(104, 45);
            this.zoomTextBox.TabIndex = 40;
            this.zoomTextBox.Value = 7;
            // 
            // zoomCheckbox
            // 
            this.zoomCheckbox.AutoSize = true;
            this.zoomCheckbox.Location = new System.Drawing.Point(13, 254);
            this.zoomCheckbox.Name = "zoomCheckbox";
            this.zoomCheckbox.Size = new System.Drawing.Size(53, 17);
            this.zoomCheckbox.TabIndex = 41;
            this.zoomCheckbox.Text = "Zoom";
            this.zoomCheckbox.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Menu;
            this.ClientSize = new System.Drawing.Size(857, 502);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.Controlli.ResumeLayout(false);
            this.Controlli.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pixelStepTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTextBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esportaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esportaFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salvaProgettoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esciToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ToolStripMenuItem nuovoProgettoToolStripMenuItem;
        private System.Windows.Forms.GroupBox Controlli;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar pixelStepTextBox;
        private System.Windows.Forms.CheckBox zoomCheckbox;
        private System.Windows.Forms.TrackBar zoomTextBox;
        private System.Windows.Forms.Button undoButton;
        private System.Windows.Forms.Button generatePatternButton;
        private System.Windows.Forms.TextBox xMaxCheckBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox xMinCheckBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox yMinCheckBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox yMaxCheckBox;
        private System.Windows.Forms.TextBox iterationCountTextBox;
    }
}

